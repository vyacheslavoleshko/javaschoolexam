# README #


[ ![Codeship Status for vyacheslavoleshko/javaschoolexam](https://app.codeship.com/projects/5e0efbb0-dbc2-0134-ee35-6ad14aa8aa6e/status?branch=master)](https://app.codeship.com/projects/204101)

Здравствуйте! Хотелось бы добавить пару слов о первом задании (Calculator).
Программа рабочая и может справляться с любым количеством вложенных скобок в выражении. Однако, алгоритм её работы основан на рекурсивных вызовах (рекурсивно обходятся все выражения в скобках, внутри каждой скобки поочерёдно выполняются все операции +-*/). В связи с этим алгоритм не оптимален. Во время выполнения задания я обнаружил, что существует более компактная и быстрая реализация такого калькулятора, решающая задачу за линейное время - http://www.geeksforgeeks.org/expression-evaluation/  
Тем не менее решил оставить пусть более громоздкое и медленное, но собственноручно написанное решение.

Чтобы посмотреть логику работы программы, раскомментируйте строку 33 в Calculator.java и запустите программу.