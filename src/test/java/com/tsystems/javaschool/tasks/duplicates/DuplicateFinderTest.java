package com.tsystems.javaschool.tasks.duplicates;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;


public class DuplicateFinderTest extends Assert{

    private DuplicateFinder duplicateFinder = new DuplicateFinder();

    @Test(expected = IllegalArgumentException.class)
    public void test() {
        //run
        duplicateFinder.process(null, new File("a.txt"));

        //assert : exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1() {
        //run
        duplicateFinder.process(new File("a.txt"), null);

        //assert : exception
    }

    @Test
    public void testIfSourceFileNotFound() throws IOException {
        String dirName = System.getenv("SystemDrive");
        String fileName = "newFile.txt";
        File f = new File(dirName, fileName);
        if (f.createNewFile()) {
            boolean result = duplicateFinder.process(new File(""), f);
            assertFalse(result);
            f.delete();
        }
    }

    @Test
    public void testIfOutputFileNotFound() throws IOException {
        String dirName = System.getenv("SystemDrive");
        String fileName = "newFile.txt";
        File f = new File(dirName, fileName);
        if (f.createNewFile()) {
            boolean result = duplicateFinder.process(f, new File(""));
            assertFalse(result);
            f.delete();
        }
    }
    @Test
    public void testIfBothFilesAreEmpty() throws IOException {
        String dirName = System.getenv("SystemDrive");
        String fileName1 = "newFile.txt";
        String fileName2 = "newFile2.txt";
        File f1 = new File(dirName, fileName1);
        File f2 = new File(dirName, fileName2);
        if (f1.createNewFile() && f2.createNewFile()) {
            boolean result = duplicateFinder.process(f1, f2);
            assertTrue(result);
            f1.delete();
            f2.delete();
        }
    }

}