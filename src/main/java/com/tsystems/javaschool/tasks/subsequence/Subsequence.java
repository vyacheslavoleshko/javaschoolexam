package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException("Arguments should not contain null values");
        }

        if (x.isEmpty()) return true;

        int index = 0;
        // O(n) complexity
        for (Object s : y) {
            if (s.equals(x.get(index))) {
                index++;
                if (index == x.size() - 1) {
                    return true;
                }
            }
        }
        return false;
    }
}
