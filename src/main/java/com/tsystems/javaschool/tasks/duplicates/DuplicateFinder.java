package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        if (sourceFile == null || targetFile == null) {
            throw new IllegalArgumentException("Null values not allowable");
        }
        TreeMap<String, Integer> treeMap = new TreeMap<>();
        try {
            try (BufferedReader br = new BufferedReader(new FileReader(sourceFile));
                FileWriter fr =new FileWriter(targetFile, true)) {
                String line;
                while ((line = br.readLine()) != null) {
                    treeMap.put(line, treeMap.containsKey(line) ? treeMap.get(line) + 1 : 1);
                }

                for (Map.Entry<String, Integer> entry : treeMap.entrySet()) {
                    String currentLine = entry.getKey();
                    Integer counter = entry.getValue();
                    fr.write(String.format("%s[%d]" + System.lineSeparator(), currentLine, counter));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        DuplicateFinder duplicateFinder = new DuplicateFinder();
        duplicateFinder.process(new File("C:\\New1.txt"), new File("C:\\New24.txt"));
    }

}
