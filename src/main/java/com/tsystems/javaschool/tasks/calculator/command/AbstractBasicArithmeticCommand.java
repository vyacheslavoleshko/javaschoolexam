package com.tsystems.javaschool.tasks.calculator.command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// if we need to perform some arithmetic operation and we need to get two operands for this,
// we can extend this abstract class
public abstract class AbstractBasicArithmeticCommand implements Command {

    // uses regex pattern to get two operands separated by operator
    public List<Float> getTwoOperands(String plainStatement, String regExPattern) {
        Pattern p = Pattern.compile(regExPattern);
        Matcher m = p.matcher(plainStatement);
        ArrayList<Float> list = new ArrayList<>();
        if (m.find()) {
            float leftOperand = Float.valueOf(m.group(1));
            float rightOperand = Float.valueOf(m.group(2));
            list.add(leftOperand);
            list.add(rightOperand);
            return list;
        } else return Collections.EMPTY_LIST;

    }

}
