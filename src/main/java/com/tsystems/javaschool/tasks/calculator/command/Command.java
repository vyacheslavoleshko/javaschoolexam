package com.tsystems.javaschool.tasks.calculator.command;

//interface for arithmetic operations
public interface Command {
    public String doOperation(String plainStatement);
}
