package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.command.*;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Stack;


public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {  // program starts from here
        if (!Validator.validateWholeStatement(statement)){  // check if whole statement is valid
            System.out.println("RESULT = null");
            return null;
        }
        System.out.println(String.format("INITIAL_STATEMENT = %s", statement));
        return evaluateWholeStatement(statement);
    }

    // this function recursively evaluates plain statements (statements inside parenthesis)
    public String evaluateWholeStatement(String statement) {
        statement = statement.replaceAll("\\s+", "");

//        System.out.println(statement); // <----- Uncomment this and run to see how program works if statement has parenthesis
        if (!statement.contains(")")) {  // exit from recursion: no parenthesis left
            String result = evaluatePlainStatement(statement);
            if (result.equals(String.valueOf(Float.POSITIVE_INFINITY)) ||
                result.equals(String.valueOf(Float.NEGATIVE_INFINITY))) {
                System.out.println("RESULT = null");
                return null;
            }
            DecimalFormat df = new DecimalFormat("#.####");
            df.setRoundingMode(RoundingMode.HALF_DOWN);
            String roundedResult =  df.format(Float.valueOf(result)).replace(",", ".");

            System.out.println(String.format("RESULT = %s", roundedResult));
            return roundedResult;  // return result
        }
        else{  // if there are parenthesis is statement
            char[] stringSymbols = statement.toCharArray();
            String plainStatement = getStatementInsideParenthesis(stringSymbols); // get plain statement inside parenthesis
            if (!Validator.validatePlainStatement(plainStatement)) {  // check if plain statement is valid
                System.out.println("RESULT = null");
                return null;
            }
            String evalPlainStatementResult = evaluatePlainStatement(plainStatement);
            // simply insert result of evaluating instead of expression in parenthesis
            String newStatement = statement.replace("(" + plainStatement + ")", evalPlainStatementResult);
            return evaluateWholeStatement(newStatement);  // do all stuff one more time
        }
    }

    public String getStatementInsideParenthesis(char[] stringSymbols) {
        Stack<Character> stack = new Stack<>();
        String statementInsideParanthesis = "";
        for (char symbol : stringSymbols) {
            if (symbol != ')') {
                stack.push(symbol); // keep pushing chars into the stack until closing parenthesis not found
            } else {
                while (true) {
                    if (stack.isEmpty()) return null; // means that parenthesis are in wrong order
                    if (stack.peek()=='('){ break;}  // when reach opening parenthesis - break while and for loops
                    else{
                        statementInsideParanthesis = stack.pop() + statementInsideParanthesis;
                    }
                }
                break;
            }
        }
        return statementInsideParanthesis;
    }

    // evaluates statement with no parenthesis
    public String evaluatePlainStatement(String plainStatement) {
        MinusCommand minusCommand = new MinusCommand();
        PlusCommand plusCommand = new PlusCommand();
        ProductCommand productCommand = new ProductCommand();
        DivisionCommand divisionCommand = new DivisionCommand();

        // Do operations in correct order. Every operation returns new string with inserted evaluated value
        String tmp1 = divisionCommand.doOperation(plainStatement);  //e.g. plainStatement= "3-2*8/4" -> tmp1 = "3-2*2"
        String tmp2 = productCommand.doOperation(tmp1);
        String tmp3 = minusCommand.doOperation(tmp2);
        String result = plusCommand.doOperation(tmp3);
        return result;
    }

    public static void main(String[] args) {
        Calculator c = new Calculator();
        c.evaluate("2 +(7 -2*(2 + 1)) - 3/3");
    }
}
